module.exports = function(app) {
    var users = require('./controllers/users');
    app.get('/users', users.findAll);
    app.get('/users/:id', users.findById);
    app.post('/users', users.add);
    app.put('/users/:id', users.update);
    app.delete('/users/:id', users.delete);

    var agenda = require('./controllers/agenda');
    app.get('/agenda', agenda.findAll);
    app.post('/agenda/user', agenda.findByUserId);
    app.post('/agenda/id', agenda.findById);


}