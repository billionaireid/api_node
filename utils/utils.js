/**
 * Created by antoine on 26/03/2017.
 */
var mysql = require('mysql');
var config = require('./config.js');


var connection = mysql.createConnection({
    host: config.database.host,
    user: config.database.user,
    password: config.database.password,
    database : config.database.db,
});

connection.connect(function(err) {
    if (err) {
        console.error('LOG ERROR - error connecting: ' + err.stack);
        return;
    }
    console.log('LOG INFO - Connection to mysql completed' );
});

module.exports = { connection: connection };