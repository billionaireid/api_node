var env = process.env.NODE_ENV || 'production';
var config = require('./myconfig')[env];

module.exports = config;