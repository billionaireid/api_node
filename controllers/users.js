'use strict';
var db = require('../utils/utils');

exports.findAll = function(req, res){
    db.connection.query('SELECT * FROM users', function (error, results, fields) {
        if (error) throw error;
        // connected!

        res.send(results);
    });
};


exports.findById = function(req, res){
    var id = req.params.id;
    db.connection.query('SELECT * FROM users WHERE id = ?', id, function (error, results, fields) {
        if (error) throw error;

        res.send(results);
    });
};


exports.add = function(req, res){
    var values = req.body;
    db.connection.query('INSERT INTO `users` SET ? ', [values] , function (error, results, fields) {
        if (error) throw error;

        res.send(results);
    });
};

exports.update = function(req, res){
    var id = req.body.mailAt;
    var name = req.body.name;
    var mail = req.body.mail;
    db.connection.query('UPDATE users SET name = ?, mail = ? WHERE mail = ?', [name, mail , id] , function (error, results, fields) {
        if (error) throw error;

        res.send(results);
    });



};

exports.delete = function(req, res){
    var id = req.params.id;
    db.connection.query('DELETE FROM users WHERE id = ?', id, function (error, results, fields) {
        if (error) throw error;

        res.send(results);
    });
};