'use strict';
var db = require('../utils/utils');

exports.findAll = function(req, res){
    db.connection.query('SELECT * FROM WORKTABLE', function (error, results, fields) {
        if (error) throw error;
        // connected!

        res.send(results);
    });
};

exports.findById = function(req, res){
    var values = req.body.id;
    db.connection.query('SELECT * FROM WORKTABLE WHERE id_worktable = ?', [values], function (error, results, fields) {
        if (error) throw error;

        res.send(results);
    });
};

exports.findByUserId = function(req, res){
    var values = req.body.user_id;
    db.connection.query('SELECT * FROM WORKTABLE WHERE USER_id_user = ?', [values], function (error, results, fields) {
        if (error) throw error;

        res.send(results);
    });
};

